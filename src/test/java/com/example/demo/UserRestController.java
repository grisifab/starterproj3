package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RequestBody;
import com.example.demo.dao.UserRepository;
import com.example.demo.models.User;


@RunWith(SpringRunner.class)
@DataJpaTest

class UserRestController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TestEntityManager entityManager;

	@Test
	void testGetUsers() {
		User p1 = new User("Dus", "JC", "14/07/1789", "Bastille", "Paris", "75007", "sans");
		User p2 = new User("Dusselier", "Jean Pierre", "24/07/1987", "Beyrouth", "Menton", "06007", "01 02 03 04 05");
		entityManager.persist(p1);
		entityManager.persist(p2);
		
		List<User> lp1 = userRepository.findAll();
		List<User> lp2 = new ArrayList<User>();
		
		for (User user : lp1) {
			lp2.add(user);
		}
		assertThat(lp2.size()).isEqualTo(2);	
	}

	@Test
	void testGetUserById() {
		User p1 = new User("Dus", "JC", "14/07/1789", "Bastille", "Paris", "75007", "sans");
		User pSavInDb = entityManager.persist(p1);
		User pFromDb = userRepository.getOne(pSavInDb.getNum());
		assertEquals(pSavInDb,pFromDb);
		assertThat(pFromDb.equals(pSavInDb));
	}

	@Test
	void testSave() {
		User p1 = new User("Dusselier", "Jean Pierre", "24/07/1987", "Beyrouth", "Menton", "06007", "01 02 03 04 05");
		User pSavInDb = entityManager.persist(p1);
		User pFromDb = userRepository.getOne(pSavInDb.getNum());
		assertEquals(pSavInDb,pFromDb);
		assertThat(pFromDb.equals(pSavInDb));
	}

	@Test
	void testUpdateUser() {
		User p1 = new User("Dusselier", "Jean Pierre", "24/07/1987", "Beyrouth", "Menton", "06007", "01 02 03 04 05");
		User pSavInDb = entityManager.persist(p1);
		User pFromDb = userRepository.getOne(pSavInDb.getNum());
		pFromDb.setNom("Tiodio");
		entityManager.persist(pFromDb);
		pFromDb = userRepository.getOne(pSavInDb.getNum());
		assertEquals(pFromDb.getNom(),"Tiodio");
	}

	@Test
	void testDeleteUser() {
		User p1 = new User("Dus", "JC", "14/07/1789", "Bastille", "Paris", "75007", "sans");
		User p2 = new User("Dusselier", "Jean Pierre", "24/07/1987", "Beyrouth", "Menton", "06007", "01 02 03 04 05");
		User pSavInDb = entityManager.persist(p1);
		entityManager.persist(p2);
		entityManager.remove(pSavInDb);
		List<User> lp1 = userRepository.findAll();
		List<User> lp2 = new ArrayList<User>();
		
		for (User user : lp1) {
			lp2.add(user);
		}
		assertThat(lp2.size()).isEqualTo(1);
	}

}
