package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Starterproj3Application {

	public static void main(String[] args) {
		SpringApplication.run(Starterproj3Application.class, args);
	}

}
