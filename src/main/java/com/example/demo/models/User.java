package com.example.demo.models;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long num;
	private String nom;
	private String prenom;
	private String dateAnniversaire;
	private String rue;
	private String ville;
	private String codePostal;
	private String telephone;
	public Long getNum() {
		return num;
	}
	public void setNum(Long num) {
		this.num = num;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getDateAnniversaire() {
		return dateAnniversaire;
	}
	public void setDateAnniversaire(String dateAnniversaire) {
		this.dateAnniversaire = dateAnniversaire;
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	@Override
	public String toString() {
		return "User [num=" + num + ", nom=" + nom + ", prenom=" + prenom + ", dateAnniversaire=" + dateAnniversaire
				+ ", rue=" + rue + ", ville=" + ville + ", codePostal=" + codePostal + ", telephone=" + telephone + "]";
	}
	public User(String nom, String prenom, String dateAnniversaire, String rue, String ville,
			String codePostal, String telephone) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateAnniversaire = dateAnniversaire;
		this.rue = rue;
		this.ville = ville;
		this.codePostal = codePostal;
		this.telephone = telephone;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}