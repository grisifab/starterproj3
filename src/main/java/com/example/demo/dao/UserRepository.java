package com.example.demo.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import com.example.demo.models.User;

public interface UserRepository extends JpaRepository<User, Long> {
//	@Query("select p from Personne p where p.nom = ?1")
//	List<Personne> chercherSelonLeNom(String nom);
//	List<Personne> findByNomAndPrenom(String nom,String prenom);
	
//	@Query("select p from Personne p where p.nom like %:x%")
//	public Page<Personne> chercher(@Param("x") String mc, Pageable pageable);
	
}