package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.dao.UserRepository;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.models.User;

@RestController
public class UserRestController {
	@Autowired
	private UserRepository userRepository;

	@GetMapping(value = "/users")
	public List<User> getUsers() {
		return userRepository.findAll();
	}

	@GetMapping(value = "/users/{id}")
	public ResponseEntity<User> getUserById(@PathVariable Long id) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not found :: " + id));
		return ResponseEntity.ok().body(user);
	}

	@PostMapping(value = "/users")
	public User save(@RequestBody User p) {
		return userRepository.save(p);
	}

	@PutMapping("/users/{id}")
    public ResponseEntity<User> updateUser(@RequestBody User p,
        @PathVariable(value = "id") Long id) throws ResourceNotFoundException {
		User user = userRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("User not found :: " + id));
		user.setNom(p.getNom());
		user.setPrenom(p.getPrenom());
        final User updatedUser = userRepository.save(user);
        return ResponseEntity.ok(updatedUser);
    }

	@DeleteMapping("/users/{id}")
	public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not found :: " + id));
		userRepository.delete(user);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}